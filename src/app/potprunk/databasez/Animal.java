package app.potprunk.databasez;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

public class Animal {

	
	public static String TableName = "Animal";
	public static String ColName = "AniName";
	public static String ColDmg = "AniDmg";
	public static String ColEva = "AniEvasion";
	public static String ColFlee = "AniFlee";
	public static String ColFleeDis = "AniFleeDis";
	public static String ColExp = "AniExp";
	public static String ColLong = "AniLong";
	public static String ColLati = "AniLati";
	public static String ColPic = "AniPic";
	
	public static int AniName=0;
	public static int AniDmg=1;
	public static int AniEva=2;
	public static int AniFlee=3;
	public static int AniFleeDis=4;
	public static int AniExp=5;
	public static int AniLong=6;
	public static int AniLati=7;
	public static int AniPic=8;
	

	public static void genAnimal(SQLiteDatabase db, Context context) {
		db.execSQL("CREATE TABLE " + Animal.TableName + " ("
				+ Animal.ColName + " TEXT PRIMARY KEY , " 
				+ Animal.ColDmg + " INTEGER, "
				+ Animal.ColEva + " REAL, "
				+ Animal.ColFlee + " REAL, "
				+ Animal.ColFleeDis + " INTEGER, "
				+ Animal.ColExp + " INTEGER, "
				+ Animal.ColLong + " Real, "
				+ Animal.ColLati + " Real, "
				+ Animal.ColPic + " TEXT );");

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					context.getAssets().open("table/Animal.csv")));
			String readLine = null;
			readLine = br.readLine();

			try {
				while ((readLine = br.readLine()) != null) {
					String[] str = readLine.split(",");
					db.execSQL("INSERT INTO " + Animal.TableName + " ("
							+ Animal.ColName + ", " 
							+ Animal.ColDmg + ", "
							+ Animal.ColEva + ", "
							+ Animal.ColFlee + ", "
							+ Animal.ColFleeDis + ", "
							+ Animal.ColExp + ", "
							+ Animal.ColLong + ", "
							+ Animal.ColLati + ", "
							+ Animal.ColPic
							+ ") VALUES ('"
							+ str[0] + "', '" 
							+ str[1] + "', '" 
							+ str[2] + "', '"
							+ str[3] + "', '"
							+ str[4] + "', '"
							+ str[5] + "', '"
							+ str[6] + "', '"
							+ str[7] + "', '"
							+ str[8] + "');");
					
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
