package app.potprunk.databasez;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class Item {
	public static String TableName = "Item";
	public static String ColName = "ItemName";
	public static String ColPrice = "ItemPrice";
	public static String ColType = "ItemType";
	public static String ColPic = "ItemPic";

	public static void genItem(SQLiteDatabase db, Context context) {
		db.execSQL("CREATE TABLE " + Item.TableName + " (" + Item.ColName
				+ " TEXT PRIMARY KEY , " + Item.ColPrice + " INTEGER, "
				+ Item.ColType + " TEXT, " + Item.ColPic + " TEXT );");

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					context.getAssets().open("table/Item.csv")));
			String readLine = null;
			readLine = br.readLine();

			try {
				while ((readLine = br.readLine()) != null) {
					String[] str = readLine.split(",");
					db.execSQL("INSERT INTO " 
							+ Item.TableName + " ("
							+ Item.ColName + ", " 
							+ Item.ColPrice + ", "
							+ Item.ColType + ", " 
							+ Item.ColPic + ") VALUES ('"
							+ str[0] + "', '" 
							+ str[1] + "', '" 
							+ str[2] + "', '" 
							+ str[3] + "');");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
