package app.potprunk.databasez;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.nsc.missoinz.R;

public class ViewItem extends Activity {
	DatabaseZ mHelper;
	SQLiteDatabase mDb;
	Cursor mCursor;
	ListView listStudent;
	TextView title_txt;
	TextView money_txt;
	TextView moneyShow;
	LinearLayout money_layout;
	Typeface typeface;
	int money_value;
	int priceInt;
	int amountOfItem;
	SharedPreferences sp;
	SharedPreferences.Editor editor;

	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.view);
		typeface = Typeface.createFromAsset(getAssets(), "fonts/Arabica.ttf");

		mHelper = new DatabaseZ(this);
		mDb = mHelper.getReadableDatabase();

		mCursor = mDb.rawQuery("SELECT * FROM " + Item.TableName, null);
		title_txt = (TextView) findViewById(R.id.title);
		money_txt = (TextView) findViewById(R.id.message);
		title_txt.setTypeface(typeface);
		money_txt.setTypeface(typeface);
		ArrayList<String> arr_list = new ArrayList<String>();
		ArrayList<Integer> resId = new ArrayList<Integer>();
		ArrayList<String> price = new ArrayList<String>();

		ArrayList<String> name_list = new ArrayList<String>();
		ArrayList<String> pic = new ArrayList<String>();
		ArrayList<String> type = new ArrayList<String>();

		mCursor.moveToFirst();
		while (!mCursor.isAfterLast()) {
			String colname = mCursor.getString(mCursor
					.getColumnIndex(Item.ColName));
			String colprice = mCursor.getString(mCursor
					.getColumnIndex(Item.ColPrice));
			arr_list.add(colname);
			// ��ػ������� arr_list.add("�׹��Ǫ�Ǻ�ҹ �Ҥ� 20 ��Դ�׹");

			name_list.add(colname);

			String datapic = mCursor.getString(mCursor
					.getColumnIndex(Item.ColPic));

			pic.add(datapic);

			int resID = getResources().getIdentifier(datapic, "drawable",
					getPackageName());
			resId.add(resID);

			price.add(colprice);

			type.add(mCursor.getString(mCursor.getColumnIndex(Item.ColType)));

			mCursor.moveToNext();
		}

		CustomAdapter adapter = new CustomAdapter(getApplicationContext(),
				arr_list, resId, price, name_list, type, pic);

		ListView listView = (ListView) findViewById(R.id.listView1);
		listView.setAdapter(adapter);

		ImageButton backButton = (ImageButton) this.findViewById(R.id.previous);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(android.R.anim.slide_in_left,
						android.R.anim.slide_out_right);
			}
		});

		moneyShow = (TextView) this.findViewById(R.id.message);
		setTextMoney();
		money_layout = (LinearLayout) findViewById(R.id.money_layout);
		int radius = 30;
		GradientDrawable gradient = new GradientDrawable();
		gradient.setShape(GradientDrawable.RECTANGLE);
		gradient.setColor(Color.rgb(177, 107, 254));
		gradient.setCornerRadii(new float[] { radius, radius, radius, radius,
				radius, radius, radius, radius });
		money_layout.setBackground(gradient);
	}

	public void setTextMoney() {
		SharedPreferences sp = getSharedPreferences("PREF_NAME2",
				Context.MODE_PRIVATE);
		moneyShow.setText("" + sp.getInt("money_value", -1));

	}

	public void onStop() {
		super.onStop();
		mHelper.close();
		mDb.close();
	}

	// new class
	public class CustomAdapter extends BaseAdapter {
		Context mContext;

		ArrayList<String> arr_list;
		ArrayList<Integer> resId;
		ArrayList<String> price;
		ArrayList<String> name_list;
		ArrayList<String> pic;
		ArrayList<String> type;

		public CustomAdapter(Context context, ArrayList<String> arr_list,
				ArrayList<Integer> resId, ArrayList<String> price,
				ArrayList<String> name_list, ArrayList<String> type,
				ArrayList<String> pic) {
			this.mContext = context;
			this.arr_list = arr_list;
			this.price = price;
			this.resId = resId;
			this.name_list = name_list;
			this.pic = pic;
			this.type = type;
		}

		public int getCount() {
			return arr_list.size();
		}

		public Object getItem(int arg0) {
			return null;
		}

		public long getItemId(int arg0) {
			return 0;
		}

		@SuppressLint("ViewHolder")
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			LayoutInflater mInflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View row = mInflater.inflate(R.layout.listview_row_store, parent,
					false);

			final Typeface typeface = Typeface.createFromAsset(getAssets(),
					"fonts/Arabica.ttf");

			TextView textView = (TextView) row.findViewById(R.id.title);
			textView.setText(arr_list.get(position));
			textView.setTypeface(typeface);

			ImageView imageView = (ImageView) row.findViewById(R.id.imageView1);
			imageView.setBackgroundResource(resId.get(position));

			String priceInBtn = price.get(position) + " �ѧ��";

			Button buyButton = (Button) row.findViewById(R.id.buy_button);
			buyButton.setTypeface(typeface);
			buyButton.setText(priceInBtn);
			buyButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					priceInt = Integer.parseInt(price.get(position));
					sp = getSharedPreferences("PREF_NAME2",
							Context.MODE_PRIVATE);
					editor = sp.edit();
					money_value = sp.getInt("money_value", -1);

					// check money that enought
					if (money_value >= priceInt) {
						WarningBuy(position);

						// old code -------------- fast to buy item
						// money_value -= priceInt;
						// Log.i("Money now", Integer.toString(money_value));
						// editor.putInt("money_value", money_value);
						// editor.commit();
						// setTextMoney();
						//
						// String item_name = name_list.get(position);
						// String item_pic = pic.get(position);
						// String item_type = type.get(position);
						//
						// Cursor mCursor = mDb.rawQuery(
						// "SELECT * FROM " + Backpack.TableName
						// + " WHERE " + Backpack.ColItemName
						// + "='" + item_name + "'", null);
						// if (mCursor.getCount() == 0) {
						// mDb.execSQL("INSERT INTO " + Backpack.TableName
						// + " (" + Backpack.ColItemName + ", "
						// + Backpack.ColAmount + ", "
						// + Backpack.ColType + ", " + Backpack.ColPic
						// + ") VALUES ('" + item_name + "', '" + "1"
						// + "', '" + item_type + "', '" + item_pic
						// + "');");
						// } else {
						// mDb.execSQL("UPDATE " + Backpack.TableName
						// + " SET " + Backpack.ColAmount + " = "
						// + Backpack.ColAmount + " + 1 WHERE "
						// + Backpack.ColItemName + " = '" + item_name
						// + "'");
						// }

					} else { // ����Թ������?-------------------------
						final Dialog dialog = new Dialog(ViewItem.this);
						dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
						dialog.setContentView(R.layout.customdialog);
						dialog.setCancelable(true);

						Button in = (Button) dialog
								.findViewById(R.id.btnIncrease);
						Button de = (Button) dialog
								.findViewById(R.id.btnDecrease);
						EditText edtAmountItem = (EditText) dialog
								.findViewById(R.id.edtAmountItem);
						in.setVisibility(View.GONE);
						de.setVisibility(View.GONE);
						edtAmountItem.setVisibility(View.GONE);

						Button button2 = (Button) dialog
								.findViewById(R.id.button2);
						button2.setTypeface(typeface);
						Button button1 = (Button) dialog
								.findViewById(R.id.button1);
						button1.setTypeface(typeface);

						button1.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								dialog.cancel();
							}
						});

						TextView textview1 = (TextView) dialog
								.findViewById(R.id.title);
						textview1.setTypeface(typeface);
						dialog.show();
					}
				}
			});

			imageView.setAnimation(AnimationUtils.loadAnimation(mContext,
					R.anim.listview_anim));
			textView.setAnimation(AnimationUtils.loadAnimation(mContext,
					R.anim.listview_anim));
			buyButton.setAnimation(AnimationUtils.loadAnimation(mContext,
					R.anim.listview_anim));

			return row;
		}

		public void WarningBuy(final int position) {
			final Dialog dialog = new Dialog(ViewItem.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.customdialog);
			dialog.setCancelable(true);

			Button in = (Button) dialog.findViewById(R.id.btnIncrease);
			Button de = (Button) dialog.findViewById(R.id.btnDecrease);
			final EditText edtAmountItem = (EditText) dialog
					.findViewById(R.id.edtAmountItem);
			edtAmountItem.setInputType(InputType.TYPE_NULL);
			
			amountOfItem = Integer.parseInt(edtAmountItem.getText()
					.toString());
			
			
			in.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					Log.i("moneyValue > priceInt",money_value+" > "+priceInt*amountOfItem);
					
					if (money_value >= priceInt * (amountOfItem+1)) {
						amountOfItem++;
						Log.i("tmp = ", ""+amountOfItem);
						edtAmountItem.setText(""+amountOfItem);
					}
				}
			});

			de.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (!(amountOfItem <= 1)) {
						amountOfItem--;
						Log.i("tmp = ", ""+amountOfItem);
						edtAmountItem.setText(""+amountOfItem);
					}
				}
			});

			Button button2 = (Button) dialog.findViewById(R.id.button2);
			button2.setTypeface(typeface);
			button2.setText("�׹�ѹ��ë���");
			Button button1 = (Button) dialog.findViewById(R.id.button1);
			button1.setTypeface(typeface);

			button1.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dialog.cancel();
				}
			});

			TextView textview1 = (TextView) dialog.findViewById(R.id.title);
			textview1.setTypeface(typeface);
			textview1.setText("��ͧ��ë��ͧ͢�ӹǹ");

	

			// **** ��ͧ�� algo �ӹǳ���� �����顴 �����Թ�ӹǹ

			button2.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					money_value -= Integer.parseInt(price.get(position))
							* amountOfItem;
					Log.i("Money now", Integer.toString(money_value));
					editor.putInt("money_value", money_value);
					editor.commit();
					setTextMoney();

					String item_name = name_list.get(position);
					String item_pic = pic.get(position);
					String item_type = type.get(position);

					Cursor mCursor = mDb.rawQuery("SELECT * FROM "
							+ Backpack.TableName + " WHERE "
							+ Backpack.ColItemName + "='" + item_name + "'",
							null);
					if (mCursor.getCount() == 0) {
						mDb.execSQL("INSERT INTO " + Backpack.TableName + " ("
								+ Backpack.ColItemName + ", "
								+ Backpack.ColAmount + ", " + Backpack.ColType
								+ ", " + Backpack.ColPic + ") VALUES ('"
								+ item_name + "', '" + amountOfItem + "', '"
								+ item_type + "', '" + item_pic + "');");
					} else {
						mDb.execSQL("UPDATE " + Backpack.TableName + " SET "
								+ Backpack.ColAmount + " = "
								+ Backpack.ColAmount + " + " + amountOfItem
								+ " WHERE " + Backpack.ColItemName + " = '"
								+ item_name + "'");
					}
					dialog.cancel();
				}
			});

			dialog.show();
		}

	}

}
