package app.potprunk.databasez;

import com.cengalabs.flatui.views.FlatButton;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.nsc.missoinz.R;

public class MainDB extends Activity {
	SharedPreferences sp;
	int money;
	String chooseGun;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.maindb);

		// -------------important code
		Typeface typeface = Typeface.createFromAsset(getAssets(),
				"fonts/Arabica.ttf");

		DatabaseZ mHelper = new DatabaseZ(this);
		SQLiteDatabase mDb = mHelper.getWritableDatabase();
		mHelper.close();
		mDb.close();

		sp = getSharedPreferences("PREF_NAME2", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sp.edit();
		// �纤�� money
		money = sp.getInt("money_value", 10000);
		money = 10000;
		editor.putInt("money_value", money);

		// �纤�� Gun
		chooseGun = sp.getString("gun_type", "�׹���");
		editor.putString("gun_type", chooseGun);
		editor.commit();

		// ----------------end important code

		// -----------------example code----------------

		// example getDataAnimal pic
		String[] kkk = mHelper.getAnimalData("��ҧ");
		Log.i("Animal data", kkk[Animal.AniPic]);

		// example setDataAnimal
		mHelper.setAnimalData("��ҧ", Animal.ColPic, "x");
		mHelper.close();
		
		// expmple use item and delete it
		mHelper.deleteIfItemOutOfBackpack();
		mHelper.decreaseItemInBackpack("�١�͡");
		mHelper.close();
		
		// example update collection when mission succeed
		mHelper.updateCollection("��ҧ");
		mHelper.close();

		// -------------end of example code-----------------

		FlatButton btnViewStore = (FlatButton) findViewById(R.id.btnStore);
		btnViewStore.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),
						ViewItem.class);
				startActivity(intent);
			}
		});

		FlatButton btnViewBackpack = (FlatButton) findViewById(R.id.btnViewBackpack);
		btnViewBackpack.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),
						ViewBackpack.class);
				startActivity(intent);
			}
		});

		FlatButton btnViewCollect = (FlatButton) findViewById(R.id.btnCollect);
		btnViewCollect.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),
						ViewCollection.class);
				startActivity(intent);
			}
		});

		FlatButton btnGetAnimal = (FlatButton) findViewById(R.id.btnGetAnimal);

		btnGetAnimal.setVisibility(View.GONE);

		btnViewStore.setTypeface(typeface);
		btnViewBackpack.setTypeface(typeface);
		btnViewCollect.setTypeface(typeface);

	}
}
