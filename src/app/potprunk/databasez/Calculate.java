package app.potprunk.databasez;

public class Calculate {

	public static int METForWalking = 2;
	public static int METForJoging = 7;

	public static float ezsy = (float) 0.75;
	public static float normal = 1;
	public static float hard = (float) 1.5;

	public static int scoreHealthy(int distance, float calorie, int calExpFromDifficultyMethod) {
		return (int) (distance * calorie + calExpFromDifficultyMethod);
	}

	public static int scoreArcade(int distance, int expAnimal) {
		return (int) (distance * expAnimal);
	}

	public static float calcCalorieBurnedPerMiniute(float MET, float weight, float minute) {
		return (float) (minute * MET * 3.5 * weight / 200);
	}
	
	public int calcExpFromDifficulty(int expFromAnimal,float difficulty){
		return (int)(expFromAnimal*difficulty);
	}

	public static int calcExpByLevel(int lv) { //level ��ҹ���� exp �������
		if (lv == 1) {
			return 0;
		} else {
			return (int) ((lv - 1) * 100 + calcExpByLevel(lv - 1));
		}
	}

	public static int calcLevel(int lv){ //�͹�������蹷���դ�� exp ��ҹ������� lv �����������
		if(lv==1){
			return 0;
		}else{
			return (int)((lv-1)*100+calcLevel(lv-1));
		}
	}
	
	public static int calcLevelByExp(int exp) {
		int level = -1;
		for (int i = 20; i > 1; i--) {
			if (exp >= calcLevel(i)) {
				level=i;
				break;
			}

		}
		return level;
	}

}
