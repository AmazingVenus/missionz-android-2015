package app.potprunk.databasez;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import com.nsc.missoinz.R;
public class ViewCollection extends Activity{
	DatabaseZ mHelper;
	SQLiteDatabase mDb;
	Cursor mCursor;
	ListView listStudent;
	TextView moneyShow;
	String playerName="pagorn";
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.view);
		Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Arabica.ttf");
		
		mHelper = new DatabaseZ(this);
		mDb = mHelper.getReadableDatabase();

		mCursor = mDb.rawQuery("SELECT * FROM " + Collection.TableName, null);
//�ѧ�����ӡ���к���Ш������Ǣͧ���������
		ArrayList<String> arr_list = new ArrayList<String>();
		ArrayList<Integer> resId = new ArrayList<Integer>();
		mCursor.moveToFirst();
		while (!mCursor.isAfterLast()) {
			arr_list.add(mCursor.getString(mCursor.getColumnIndex(Collection.ColAniName))
					+ " \u2715 "
					+ mCursor.getString(mCursor.getColumnIndex(Collection.ColAmount)));
			// ��ػ������� arr_list.add("�׹��Ǫ�Ǻ�ҹ �Ҥ� 20 ��Դ�׹");

			String datapic = mCursor.getString(mCursor
					.getColumnIndex(Collection.ColPic));
			int resID = getResources().getIdentifier(datapic, "drawable",
					getPackageName());
			resId.add(resID);
			
			
			
			
			mCursor.moveToNext();
		}
		

		
		

		CustomAdapter adapter = new CustomAdapter(getApplicationContext(),
				arr_list, resId);

		ListView listView = (ListView) findViewById(R.id.listView1);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

			}
		});
		
		ImageButton backButton = (ImageButton)this.findViewById(R.id.previous);
		backButton.setOnClickListener(new OnClickListener() {
		  @Override
		  public void onClick(View v) {
		    finish();
		    overridePendingTransition(android.R.anim.slide_in_left,
	                android.R.anim.slide_out_right);
		  }
		});
		
		TextView title = (TextView)this.findViewById(R.id.title);
		title.setText("Collection");
		title.setTypeface(typeface);
		
		TextView textMoney = (TextView)this.findViewById(R.id.message);
		textMoney.setVisibility(View.GONE);
		
		ImageView imgMoney = (ImageView)this.findViewById(R.id.imageView1);
		imgMoney.setVisibility(View.GONE);
		
		
	}

	public void onStop() {
		super.onStop();
		mHelper.close();
		mDb.close();
	}
	
	
	
	
	
	
	//new class
	public class CustomAdapter extends BaseAdapter {
		Context mContext;

		ArrayList<String> arr_list;
		ArrayList<Integer> resId;

		public CustomAdapter(Context context, ArrayList<String> arr_list,
				ArrayList<Integer> resId) {
			this.mContext = context;
			this.arr_list = arr_list;
			this.resId = resId;
		}

		public int getCount() {
			return arr_list.size();
		}

		public Object getItem(int arg0) {
			return null;
		}

		public long getItemId(int arg0) {
			return 0;
		}

		@SuppressLint("ViewHolder")
		public View getView(final int position, View convertView, ViewGroup parent) {
			LayoutInflater mInflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View row = mInflater.inflate(R.layout.listview_row_store, parent, false);

			Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Arabica.ttf");
			
			TextView textView = (TextView) row.findViewById(R.id.title);
			textView.setText(arr_list.get(position));
			textView.setTypeface(typeface);

			ImageView imageView = (ImageView) row.findViewById(R.id.imageView1);
			imageView.setBackgroundResource(resId.get(position));
			
			Button buyButton = (Button)row.findViewById(R.id.buy_button);
			buyButton.setVisibility(View.GONE);
			
			
			
			
			
			
			
			 imageView.setAnimation(AnimationUtils.loadAnimation(mContext
		               , R.anim.listview_anim));
			 textView.setAnimation(AnimationUtils.loadAnimation(mContext
		               , R.anim.listview_anim));

			return row;
		}
	}

}


