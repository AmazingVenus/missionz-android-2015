package app.potprunk.databasez;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.Random;

class DatabaseZ extends SQLiteOpenHelper {
	private static String DB_NAME = "DatabaseZ";
	private static int DB_VERSION = 1;
	SQLiteDatabase db;
	Cursor mCursor;
	Context context;

	public DatabaseZ(Context context) {
		super(context, DB_NAME, null, DB_VERSION);

		this.context = context;
	}

	public void onCreate(SQLiteDatabase db) {
		this.db = db;
		Item.genItem(db, context);
		Animal.genAnimal(db, context);
		Backpack.genBackpack(db, context);
		Collection.genCollection(db, context);
		

		// ������駷��ŧ app ��ҹ�� �з���������� maindb
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + Item.TableName);
		db.execSQL("DROP TABLE IF EXISTS " + Animal.TableName);
		db.execSQL("DROP TABLE IF EXISTS " + Backpack.TableName);
		db.execSQL("DROP TABLE IF EXISTS " + Collection.TableName);
		onCreate(db);
	}

	public String[] getAnimalData(String AniName) {
		String cmd = "SELECT * FROM Animal WHERE " + Animal.ColName + " = "
				+ AniName;
		try {
			String arrData[] = null;

			db = this.getReadableDatabase();

			mCursor = db.query(Animal.TableName, new String[] { "*" },
					Animal.ColName + "=?",
					new String[] { String.valueOf(AniName) }, null, null, null,
					null);

			if (mCursor != null) {
				if (mCursor.moveToFirst()) {
					arrData = new String[mCursor.getColumnCount()];
					/*
					 * 0 AniName 1 AniDmg 2 AniEvasion 3 AniFlee 4 AniFleeDis 5
					 * AniExp 6 AniLong 7 AniLati 8 AniPic
					 */
					Log.i("Amount of field", "" + mCursor.getColumnCount());

					for (int i = 0; i < mCursor.getColumnCount(); i++) {
						arrData[i] = mCursor.getString(i);
					}
					mCursor.close();
					db.close();
					return arrData;

				}
			}
		} catch (Exception e) {
			Log.i("Error", e.toString());
			return null;
		}
		return null;
	}

	public void setAnimalData(String AniName, String Column, String NewData) {
		try {
			db = this.getWritableDatabase();
			ContentValues Val = new ContentValues();
			Val.put(Column, NewData);

			db.update(Animal.TableName, Val, Animal.ColName + "=?",
					new String[] { String.valueOf(AniName) });
			db.close();

		} catch (Exception e) {
			Log.i("Error setAnimalData", "" + e);
		}
	}

	public void updateItemInBackpackBySP(String itemName, int itemAmount) {
		db = this.getWritableDatabase();
		db.execSQL("UPDATE " + Backpack.TableName + " SET "
				+ Backpack.ColAmount + " = " + itemAmount + " WHERE "
				+ Backpack.ColItemName + " = " + itemName);
		db.close();
	}

	// ��ͧ������ǵ�Ǩ�ͺ��� ����� 0 ���ź�͡�ҡ backpack ���
	public void decreaseItemInBackpack(String itemName) {
		db = this.getWritableDatabase();
		db.execSQL("UPDATE " + Backpack.TableName + " SET "
				+ Backpack.ColAmount + " = " + Backpack.ColAmount + " - 1"
				+ " WHERE " + Backpack.ColItemName + " = '" + itemName + "'");
		db.close();
	}

	public void deleteIfItemOutOfBackpack() {
		db = this.getReadableDatabase();
		db.execSQL("DELETE FROM " + Backpack.TableName + " WHERE "
				+ Backpack.ColAmount + " <= 0");
	}

	public void updateCollection(String AnimalName) {
		String colpic = "";
		db = this.getWritableDatabase();

		// get animal pic from Animal Table
		mCursor = db.rawQuery("SELECT * FROM " + Animal.TableName + " WHERE "
				+ Animal.ColName + " = '" + AnimalName + "'", null);
		mCursor.moveToFirst();
		while (!mCursor.isAfterLast()) {
			colpic = mCursor.getString(mCursor.getColumnIndex(Animal.ColPic));
			mCursor.moveToNext();
		}

		// check if collection already have animal
		mCursor = db.rawQuery(
				"SELECT * FROM " + Collection.TableName + " WHERE "
						+ Collection.ColAniName + " = '" + AnimalName + "'",
				null);

		Log.i("pass?", "pass1");

		if (mCursor.getCount() == 0) {
			db.execSQL("INSERT INTO " + Collection.TableName + " ("
					+ Collection.ColAniName + " , " + Collection.ColAmount
					+ " , " + Collection.ColPic + " ) " + "VALUES ('"
					+ AnimalName + "', '" + "1', '" + colpic + "');");

		} else {
			db.execSQL("UPDATE " + Collection.TableName + " SET "
					+ Collection.ColAmount + " = " + Collection.ColAmount
					+ " +  1 WHERE " + Collection.ColAniName + " = '"
					+ AnimalName + "'");
		}

	}
	
	
	
	public String randomSomething(String tableName,String columnName){
		db = this.getReadableDatabase();
		ArrayList<String> arrListRandom = null;
		mCursor = db.rawQuery("SELECT * FROM " + tableName, null);
		mCursor.moveToFirst();
		while (!mCursor.isAfterLast()) {
			arrListRandom.add(mCursor.getString(mCursor
					.getColumnIndex(columnName)));
			mCursor.moveToNext();
		}
		Random rand = new Random();
		int randomNum = rand.nextInt((arrListRandom.size() - 0) + 1) + 0;
		Log.i("random",""+randomNum);
		
		return arrListRandom.get(randomNum);
	}
	public String randomItem(){
		return "";
	}
}
