package app.potprunk.databasez;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class Collection {
	public static String TableName = "Collection";
	public static String ColID = "CollectID";
	public static String ColAniName = "AniName";
	public static String ColAmount = "Amount";
	public static String ColPic = "AniPic";

	public static void genCollection(SQLiteDatabase db, Context context) {
		db.execSQL("CREATE TABLE " + Collection.TableName + " (" 
				+ Collection.ColID+ "  INTEGER PRIMARY KEY AUTOINCREMENT , " 
				+ Collection.ColAniName + " TEXT, " 
				+ Collection.ColAmount + " INTEGER, "
				+ Collection.ColPic + " TEXT );");
		
/*		db.execSQL("INSERT INTO " + Collection.TableName + " (" 
				+ Collection.ColAniName + " , " 
				+ Collection.ColAmount + " , "
				+ Collection.ColPic + " ) "
				+ "VALUES ('��','1','x');"
				
				);*/

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					context.getAssets().open("table/Collection.csv")));
			String readLine = null;
			readLine = br.readLine();

			try {
				while ((readLine = br.readLine()) != null) {
					String[] str = readLine.split(",");
					db.execSQL("INSERT INTO " + Collection.TableName + " ("
							+ Collection.ColID + ", " 
							+ Collection.ColAniName + ", "
							+ Collection.ColAmount + ", "
							+ Collection.ColPic + ") VALUES ('"
							+ str[0] + "', '" 
							+ str[1] + "', '" 
							+ str[2] + "', '"
							+ str[3] + "');");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
