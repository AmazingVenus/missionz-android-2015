package app.potprunk.databasez;

import java.util.ArrayList;

import com.cengalabs.flatui.FlatUI;
import com.cengalabs.flatui.views.FlatButton;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import com.nsc.missoinz.R.color;
import com.nsc.missoinz.R;

public class ViewBackpack extends Activity {
	DatabaseZ mHelper;
	SQLiteDatabase mDb;
	Cursor mCursor;
	ListView listStudent;
	TextView moneyShow;
	LinearLayout money_layout;


	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.view);
		Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Arabica.ttf");
		
		mHelper = new DatabaseZ(this);
		mDb = mHelper.getReadableDatabase();

		mCursor = mDb.rawQuery("SELECT * FROM " + Backpack.TableName, null);
//�ѧ�����ӡ���к���Ш������Ǣͧ���������
		ArrayList<String> arr_list = new ArrayList<String>();
		ArrayList<Integer> resId = new ArrayList<Integer>();
		ArrayList<String> arr_type = new ArrayList<String>();
		mCursor.moveToFirst();
		while (!mCursor.isAfterLast()) {
			arr_list.add(mCursor.getString(mCursor.getColumnIndex(Backpack.ColItemName))
					+ " \u2715 "
					+ mCursor.getString(mCursor.getColumnIndex(Backpack.ColAmount)));
			// ��ػ������� arr_list.add("�׹��Ǫ�Ǻ�ҹ �Ҥ� 20 ��Դ�׹");

			String datapic = mCursor.getString(mCursor
					.getColumnIndex(Backpack.ColPic));
			int resID = getResources().getIdentifier(datapic, "drawable",
					getPackageName());
			resId.add(resID);
			
			arr_type.add(mCursor.getString(mCursor.getColumnIndex(Backpack.ColType)));
			
			
			
			mCursor.moveToNext();
		}
		

		
		

		CustomAdapter adapter = new CustomAdapter(getApplicationContext(),
				arr_list, resId, arr_type);

		ListView listView = (ListView) findViewById(R.id.listView1);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

			}
		});
		
		ImageButton backButton = (ImageButton)this.findViewById(R.id.previous);
		backButton.setOnClickListener(new OnClickListener() {
		  @Override
		  public void onClick(View v) {
		    finish();
		    overridePendingTransition(android.R.anim.slide_in_left,
	                android.R.anim.slide_out_right);
		  }
		});
		
		TextView title = (TextView)this.findViewById(R.id.title);
		title.setText("Backpack");
		title.setTypeface(typeface);
		title.setTextSize(30);
		
		SharedPreferences sp = getSharedPreferences("PREF_NAME2", Context.MODE_PRIVATE);
		int money_now = sp.getInt("money_value", -1);
		TextView textMoney = (TextView)this.findViewById(R.id.message);
		Log.i("money_now",""+money_now);
		textMoney.setText(""+money_now);
		textMoney.setTypeface(typeface);
		money_layout = (LinearLayout) findViewById(R.id.money_layout);
		
		int radius = 30;
		GradientDrawable gradient = new GradientDrawable();
		gradient.setShape(GradientDrawable.RECTANGLE);
		gradient.setColor(Color.rgb(177,107,254));
		gradient.setCornerRadii(new float [] {  radius,  radius, radius, radius, radius, radius,  radius,  radius});
		money_layout.setBackground(gradient);
		
	}

	public void onStop() {
		super.onStop();
		mHelper.close();
		mDb.close();
	}
	
	
	
	
	
	
	//new class
	public class CustomAdapter extends BaseAdapter {
		Context mContext;

		ArrayList<String> arr_list;
		ArrayList<Integer> resId;
		ArrayList<String> arr_type;

		public CustomAdapter(Context context, ArrayList<String> arr_list,
				ArrayList<Integer> resId, ArrayList<String> arr_type) {
			this.mContext = context;
			this.arr_list = arr_list;
			this.resId = resId;
			this.arr_type=arr_type;
		}

		public int getCount() {
			return arr_list.size();
		}

		public Object getItem(int arg0) {
			return null;
		}

		public long getItemId(int arg0) {
			return 0;
		}

		@SuppressLint("ViewHolder")
		public View getView(final int position, View convertView, ViewGroup parent) {
			LayoutInflater mInflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View row = mInflater.inflate(R.layout.listview_row_store, parent, false);

			Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Arabica.ttf");
			
			TextView textView = (TextView) row.findViewById(R.id.title);
			textView.setText(arr_list.get(position));
			textView.setTypeface(typeface);
			

			
			

			ImageView imageView = (ImageView) row.findViewById(R.id.imageView1);
			imageView.setBackgroundResource(resId.get(position));
			
			FlatButton useButton = (FlatButton)row.findViewById(R.id.buy_button);
			
			useButton.setText("��ҹ");
			useButton.getAttributes().setTheme(FlatUI.CANDY, getResources());
			useButton.setTypeface(typeface);
			
			final String tmpType = arr_type.get(position);

			//�ѧ�����Ѳ������ա���ع����檹Դ
			if(tmpType.equals("����ع")){
				useButton.setVisibility(View.GONE);
			}
			
			
			
			useButton.setOnClickListener(new OnClickListener() {
		
				@Override
				public void onClick(View v) {
					SharedPreferences sp = getSharedPreferences("PREF_NAME2",
							Context.MODE_PRIVATE);
					SharedPreferences.Editor editor = sp.edit();
			
					String []tmpName=arr_list.get(position).split("\n");
					editor.putString("gun_type", tmpName[0]);
					editor.commit();
					
					
				}
			});
			
			 imageView.setAnimation(AnimationUtils.loadAnimation(mContext
		               , R.anim.listview_anim));
			 textView.setAnimation(AnimationUtils.loadAnimation(mContext
		               , R.anim.listview_anim));
			 useButton.setAnimation(AnimationUtils.loadAnimation(mContext
		               , R.anim.listview_anim));

			return row;
		}
	}

}
