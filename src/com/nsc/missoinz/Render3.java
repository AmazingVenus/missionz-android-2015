package com.nsc.missoinz;


import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

@TargetApi(Build.VERSION_CODES.KITKAT)
public class Render3 extends View implements OnTouchListener{
	Paint p,p2,p3;
	public static float x,y,z;
	private float vX;
	private float vY;
	private float cX;
	private float cY;
	public static float rx, ry;
	private int r,g,b;
	int w,h;
	private float timer;
	Bitmap cross;
	Bitmap cross_aim1;
	public static boolean isOnTarget;
	public Render3(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		p = new Paint();
		p2 = new Paint();
		p3 = new Paint();
		r = (int) (Math.random()*255);
		g = (int) (Math.random()*255);
		b = (int) (Math.random()*255);
		timer = 0;
		cross = BitmapFactory.decodeResource(getResources(), R.drawable.crosshair_normal300);
		cross_aim1 = BitmapFactory.decodeResource(getResources(), R.drawable.crosshair_red_aim300);
		this.setOnTouchListener(this);
	}
	
	@Override
	protected void onDraw(Canvas canvas){
		p.setTextSize(30);
		w = canvas.getWidth();
		h = canvas.getHeight();
		
		///Draw Target Circle
		
		
		vX = (float) ((x*40+w/2-cX+rx)*0.10);
		vY = (float) ((z*40+h/2-cY+ry)*0.10);
		cX += vX;
		cY += vY;
		if(Math.abs(cX-(w/2)) < 30 && Math.abs(cY-(h/2)) < 30){
			r = (int) (Math.random()*255);
			g = (int) (Math.random()*255);
			b = (int) (Math.random()*255);
			//blink(canvas,true);
			blink_cross(cross_aim1, cross, canvas, (int)cX, (int)cY, true);
			p3.setColor(Color.argb(50, 255, 186, 0));
			isOnTarget = true;
			//p.setColor(Color.argb(100,r, g, b));
		}else{
			//blink(canvas,false);
			blink_cross(cross_aim1, cross, canvas, (int)cX, (int)cY, false);
			//p.setColor(Color.argb(100,50, 50, 50));
			isOnTarget = false;
			p3.setColor(Color.argb(50, 255, 255, 0));
			
		}
		canvas.drawCircle(w/2, h/2, 120, p3);	
		//canvas.drawCircle(cX, cY, 100, p);
		invalidate();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		switch(event.getAction()){
		
		case MotionEvent.ACTION_DOWN :
//			rx = (float) (Math.random()*300);
//			ry = (float) (Math.random()*300);
			break;
		}
		return false;
	}
	
	private void blink(Canvas canvas, boolean isStop) {
		p.setStyle(Paint.Style.STROKE);
		p.setStrokeWidth(7);
		if (isStop) {
			if (timer < 1) {
				p.setColor(Color.rgb(255, 127, 39));
				timer += 0.1f;
			} else if (timer < 2) {
				p.setColor(Color.rgb(136, 0, 21));
				timer += 0.1f;
			} else {
				timer = 0;
			}
		}else{
			p.setColor(Color.rgb(255, 127, 39));
		}
		canvas.drawCircle(w / 2, h / 2, 120, p);
		p.setStyle(Paint.Style.FILL);
	}
	
	private void blink_cross(Bitmap b1,Bitmap b2,Canvas canvas, int cx,int cy,boolean isStop) {
		//p.setStyle(Paint.Style.FILL);
		if (isStop) {
			if (timer < 1) {
				p.setColor(Color.argb(50, 255, 0, 0));
				canvas.drawBitmap(b1, null, new Rect((int)cX-100, (int)cY-100, (int)cX+100, (int)cY+100), p2);
				timer += 0.2f;
			} else if (timer < 2) {
				p.setColor(Color.argb(30, 255, 0, 0));
				canvas.drawBitmap(b2, null, new Rect((int)cX-100, (int)cY-100, (int)cX+100, (int)cY+100), p2);
				timer += 0.2f;
				
			} else {
				canvas.drawBitmap(b2, null, new Rect((int)cX-100, (int)cY-100, (int)cX+100, (int)cY+100), p2);
				timer = 0f;
			}
			//canvas.drawBitmap(b1, null, new Rect((int)cX-100, (int)cY-100, (int)cX+100, (int)cY+100), p2);
		}else{
			
			p.setColor(Color.argb(50, 100, 100, 100));
			
			canvas.drawBitmap(b2, null, new Rect((int)cX-100, (int)cY-100, (int)cX+100, (int)cY+100), p2);
			//timer2 = 0f;
		}
		
		canvas.drawCircle(cx, cy, 100, p);
	}
}
