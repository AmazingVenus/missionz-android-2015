package com.nsc.missoinz;

import com.cengalabs.flatui.views.FlatButton;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ModeSelection extends Activity {
	private Button health_btn;
	private Button arcade_btn;
	TextView t1;
	TextView t2;
	TextView t3;
	TextView t4;
	TextView t5;
	TextView t6;
	Typeface font;
	boolean isHealth;
	SharedPreferences sp;
	Editor editor;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_mode);
		
		sp = getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);
		editor = sp.edit();
		health_btn = (Button) findViewById(R.id.done_btn);
		arcade_btn = (Button) findViewById(R.id.flatButton2);
		t1 = (TextView) findViewById(R.id.test);
		t2 = (TextView) findViewById(R.id.message);
		t3 = (TextView) findViewById(R.id.textView3);
		t4 = (TextView) findViewById(R.id.textView4);
		t5 = (TextView) findViewById(R.id.textView5);
		t6 = (TextView) findViewById(R.id.textView6);

		font = Typeface.createFromAsset(getAssets(), "fonts/Arabica.ttf");
		t1.setTypeface(font);
		t2.setTypeface(font);
		t3.setTypeface(font);
		t4.setTypeface(font);
		t5.setTypeface(font);
		t5.setTextColor(Color.WHITE);
		t6.setTypeface(font);
		t6.setTextColor(Color.WHITE);

		health_btn.setTypeface(font);
		arcade_btn.setTypeface(font);

		health_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				isHealth = true;
				editor.putString("MODE", "HEALTH");
				editor.commit();
				displayDialog();

				// Intent i = new Intent(getApplicationContext(),
				// HealthMode.class);
				// startActivity(i);
			}
		});

		arcade_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				isHealth = false;
				editor.putString("MODE", "ARCADE");
				editor.commit();
				displayDialog();
				
				// final Dialog dialog = new Dialog(ModeSelection.this);
				// dialog.setContentView(R.layout.mode_option);
				// TextView title =
				// (TextView)dialog.findViewById(R.id.textView1);
				// FlatButton easy_btn =
				// (FlatButton)dialog.findViewById(R.id.flatButton1);
				// FlatButton medium_btn =
				// (FlatButton)dialog.findViewById(R.id.flatButton2);
				// FlatButton hard_btn =
				// (FlatButton)dialog.findViewById(R.id.flatButton3);
				// easy_btn.setTypeface(font);
				// medium_btn.setTypeface(font);
				// hard_btn.setTypeface(font);
				// title.setTypeface(font);
				// dialog.show();
				// Intent i = new Intent(getApplicationContext(),
				// ArcadeMode.class);
				// startActivity(i);
			}
		});

	}

	private void displayDialog() {
		final Dialog dialog = new Dialog(ModeSelection.this);
		dialog.setContentView(R.layout.mode_option);
		TextView title = (TextView) dialog.findViewById(R.id.test);
		FlatButton easy_btn = (FlatButton) dialog
				.findViewById(R.id.done_btn);
		FlatButton medium_btn = (FlatButton) dialog
				.findViewById(R.id.flatButton2);
		FlatButton hard_btn = (FlatButton) dialog
				.findViewById(R.id.flatButton3);

		easy_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isHealth) {
					Intent i = new Intent(getApplicationContext(),
							HealthMode.class);
					editor.putInt("MODE_OPTION", 1);
					editor.commit();
					dialog.dismiss();
					startActivity(i);
					
				} else {
					Intent i = new Intent(getApplicationContext(),
							ArcadeMode.class);
					editor.putInt("MODE_OPTION", 1);
					editor.commit();
					dialog.dismiss();
					startActivity(i);
				}
			}
		});

		medium_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isHealth) {
					Intent i = new Intent(getApplicationContext(),
							HealthMode.class);
					editor.putInt("MODE_OPTION", 2);
					editor.commit();
					dialog.dismiss();
					startActivity(i);
				} else {
					Intent i = new Intent(getApplicationContext(),
							ArcadeMode.class);
					editor.putInt("MODE_OPTION", 2);
					editor.commit();
					dialog.dismiss();
					startActivity(i);
				}
			}
		});

		hard_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isHealth) {
					Intent i = new Intent(getApplicationContext(),
							HealthMode.class);
					editor.putInt("MODE_OPTION", 3);
					editor.commit();
					dialog.dismiss();
					startActivity(i);
				} else {
					Intent i = new Intent(getApplicationContext(),
							ArcadeMode.class);
					editor.putInt("MODE_OPTION", 3);
					editor.commit();
					dialog.dismiss();
					startActivity(i);
				}
			}
		});

		easy_btn.setTypeface(font);
		medium_btn.setTypeface(font);
		hard_btn.setTypeface(font);
		title.setTypeface(font);
		dialog.show();

	}
}
