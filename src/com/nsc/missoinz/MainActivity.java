package com.nsc.missoinz;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import app.potprunk.databasez.MainDB;

import com.cengalabs.flatui.views.FlatButton;

public class MainActivity extends Activity {

	private Button open_map;
	private Button setting;
	private FlatButton reset;
	SharedPreferences sp;
	SharedPreferences.Editor editor;
	//private ButtonImageWithin test;
	double lat, lng;
	boolean isInternetPresent = false;
	ConnectionDetector cd;
	Typeface font;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		cd = new ConnectionDetector(getApplicationContext());
		///Check internet connection
		// get Internet status
		
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (!isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
            showAlertDialog(MainActivity.this, "No Internet Connection",
                    "You don't have internet connection. Open Wi-fi for internet connection?", false);
        }
		///
        
		font = Typeface.createFromAsset(getAssets(),
				"fonts/Arabica.ttf");
		sp = getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);
		editor = sp.edit();
		setting = (Button) findViewById(R.id.button1);
		setting.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(),
						MainDB.class);
				startActivity(i);
			}
		});
		open_map = (Button) findViewById(R.id.button2);
		open_map.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (sp.getBoolean("IS_FIRST_RUN", true) == true) {
					Intent i = new Intent(getApplicationContext(),
							EnterName.class);
					startActivity(i);
				} else {
					

					 Intent i = new Intent(getApplicationContext(),
					 ModeSelection.class);
					 startActivity(i);
				}
			}
		});

		reset = (FlatButton) findViewById(R.id.done_btn);
		reset.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editor.putBoolean("IS_FIRST_RUN", true);
				editor.commit();
			}
		});
		
		/// Test Dialog
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.missoin_complete);
		String[] list = { "Aerith Gainsborough", "Barret Wallace", "Cait Sith"
                , "Cid Highwind"};
		int resId = R.drawable.calorie_icon;
		MyAdapter adapter = new MyAdapter(getApplicationContext(), list, resId,font);
		ListView listView = (ListView)dialog.findViewById(R.id.listView1);
        listView.setAdapter(adapter);
        
		//dialog.show();
		
//		test.setText("Start Game");
//		test.setButtonColor(color.blossom_dark);
//		test.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				if (sp.getBoolean("IS_FIRST_RUN", true) == true) {
//					Intent i = new Intent(getApplicationContext(),
//							EnterName.class);
//					startActivity(i);
//				} else {
//					
//
//					 Intent i = new Intent(getApplicationContext(),
//					 ModeSelection.class);
//					 startActivity(i);
//				}
//			}
//		});
		
	}
	public void onResume() {
		super.onResume();
//		cd = new ConnectionDetector(getApplicationContext());
//		///Check internet connection
//		// get Internet status
//		
//        isInternetPresent = cd.isConnectingToInternet();
//
//        // check for Internet status
//        if (!isInternetPresent) {
//            // Internet Connection is Present
//            // make HTTP requests
//            showAlertDialog(MainActivity.this, "No Internet Connection",
//                    "You don't have internet connection. Open Wi-fi for internet connection?", false);
//        }
		///
		
	}
	public void showAlertDialog(Context context, String title, String message, final Boolean status) {
//        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
// 
//        // Setting Dialog Title
//        alertDialog.setTitle(title);
// 
//        // Setting Dialog Message
//        alertDialog.setMessage(message);
//         
//        // Setting alert dialog icon
//        alertDialog.setIcon((status) ? R.drawable.success_icon : R.drawable.fail_icon);
// 
//        // Setting OK Button
//        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//            }
//        });
// 
//        // Showing Alert Message
//        alertDialog.show();
		
		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.connection_dialog);
		TextView title_txt = (TextView)dialog.findViewById(R.id.title);
		TextView msg_txt = (TextView)dialog.findViewById(R.id.message);
		FlatButton conect_ok_btn = (FlatButton)dialog.findViewById(R.id.connect_ok);
		ImageView icon = (ImageView)dialog.findViewById(R.id.imageView1);
		icon.setImageResource((status) ? R.drawable.success_icon : R.drawable.fail_icon);
		title_txt.setText(title);
		title_txt.setTypeface(font);
		msg_txt.setText(message);
		//msg_txt.setTypeface(font);
		
		conect_ok_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!status)
					startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
				dialog.dismiss();
			}
		});
		dialog.show();
    }
}
