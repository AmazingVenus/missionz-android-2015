package com.nsc.missoinz;

import com.nsc.missoinz.R.color;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class Shooting extends View{

	public static int damage;
	public static boolean isShoot;
	float delay;
	Paint p;
	public Shooting(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		p = new Paint();
		delay = 10f;
	}

	@Override
	protected void onDraw(Canvas canvas){
		
		p.setTextSize(30);
		p.setColor(color.blossom_light);
		if(isShoot){
			if(delay > 0)
				canvas.drawText(""+damage, 0, 0, p);
			delay -= 0.1f;
		}
		
		isShoot = false;
		invalidate();
	}
}
