package com.nsc.missoinz;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public class Animal {
	private int health;
	private int exp;
	private Marker pos;
	private int level;
	private String name;
	
	public Animal(String name,int hp,int exp,int lvl,Marker p){
		this.health = hp;
		this.exp = exp;
		this.pos = p;
		this.level = lvl;
		this.name = name;
	}
	
	public void setAnimalHealth(int hp){
		this.health = hp;
	}
	public int getAnimalHealth(){
		return this.health;
	}
	
	public void setAnimalExp(int exp){
		this.exp = exp;
	}
	public int getAnimalExp(){
		return this.exp;
	}
	
	public void setAnimalLevel(int level){
		this.level = level;
	}
	public int getAnimalLevel(){
		return this.level;
	}
	
	public void setAnimalPos(Marker marker){
		this.pos = marker;
	}
	public Marker getAnimalPos(){
		return pos;
	}
	
	public void setAnimalName(String name){
		this.name = name;
	}
	public String getAnimalName(){
		return this.name;
	}
}
