package com.nsc.missoinz;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class AnimalRender extends View{

	Paint paint;
	int[] resID;
	float deltaTime;
	float frameIndex;
	Bitmap[] animalFrame = new Bitmap[2];
	Rect rec;
	public AnimalRender(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		paint = new Paint();
		frameIndex = 0;
		animalFrame[0] = BitmapFactory.decodeResource(getResources(), R.drawable.lion_normal_1);
		animalFrame[1] = BitmapFactory.decodeResource(getResources(), R.drawable.lion_normal_2);
		rec = new Rect(256, 256, 256, 256);
	}
	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas){
		int w = canvas.getWidth();
		int h = canvas.getHeight();
		paint.setTextSize(20);
		paint.setColor(Color.CYAN);
		rec.set(w/2-256, h/2-256, w/2+256, h/2+256);
		canvas.drawBitmap(animalFrame[(int) frameIndex],null, rec, paint);
		frameIndex += 0.1;
		if(frameIndex >= 2){
			frameIndex = 0;
		}
		invalidate();
	}

}
