package com.nsc.missoinz;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAdapter extends BaseAdapter{
	Context mContext;
    String[] strName; 
    int resId;
    Typeface font;
    public MyAdapter(Context context, String[] strName, int resId,Typeface font) {
        this.mContext= context;
        this.strName = strName;
        this.resId = resId;
        this.font = font;
    }
    
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return strName.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater mInflater = 
                (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
     
        View row = mInflater.inflate(R.layout.listview_row, parent, false);
        
        TextView textView = (TextView)row.findViewById(R.id.title);
        textView.setText(strName[position]);
        textView.setTypeface(font);
        
        ImageView imageView = (ImageView)row.findViewById(R.id.imageView1);
        imageView.setBackgroundResource(resId);
        
		return row;
	}

}
