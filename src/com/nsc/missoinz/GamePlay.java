package com.nsc.missoinz;

import java.util.List;
import java.util.Random;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.IconRoundCornerProgressBar;
import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.cengalabs.flatui.views.FlatButton;
import com.cengalabs.flatui.views.FlatTextView;

@SuppressLint("NewApi")
@SuppressWarnings("deprecation")
public class GamePlay extends Activity implements SurfaceHolder.Callback {
	SharedPreferences sp;
	SharedPreferences.Editor editor;
	Camera mCamera;
    SurfaceView mPreview;
    SensorManager sensorManager;
	Sensor sensor;
	FlatTextView x_axis;
	FlatTextView y_axis;
	FlatTextView z_axis;
	FlatTextView sc;
	ImageView animal;
	Button shoot;
	Render3 ss;
	AnimalRender animal2;
	//Shooting shooting;
	double fx,fy;
	double PI = Math.PI;
	int animal_hp = 150;
	IconRoundCornerProgressBar animal_hp_bar;
	IconRoundCornerProgressBar hp_bar;
	IconRoundCornerProgressBar mana_bar;
	RoundCornerProgressBar exp_bar;
	int mana;
	int hp;
	int exp;
	int hp_max;
	int mana_max;
	int exp_max;
	TextView tt;
	Animation anim;
	Animation anim2;
	Typeface font;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN 
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.ingame);
        sp = getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);
		editor = sp.edit();
        mPreview = (SurfaceView)findViewById(R.id.surfaceView1);
        mPreview.getHolder().addCallback(this);
        mPreview.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        font = Typeface.createFromAsset(getAssets(),
    			"fonts/Arabica.ttf");
        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
		sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		shoot = (Button)findViewById(R.id.done_btn);
		animal = (ImageView)findViewById(R.id.imageView1);
		ss = (Render3)findViewById(R.id.view1);
		tt = (TextView)findViewById(R.id.test);
		anim = AnimationUtils.loadAnimation(this, R.anim.anim_1);
		tt.setVisibility(View.INVISIBLE);
		//shooting = (Shooting)findViewById(R.id.view2);
		
		hp_max = sp.getInt("PLAYER_MAX_HP", 100);
		mana_max = sp.getInt("PLAYER_MAX_STA", 100);
		exp_max = sp.getInt("PLAYER_MAX_EXP", 100);
		mana = sp.getInt("PLAYER_CUR_STA", 100);
		hp = sp.getInt("PLAYER_CUR_HP", 100);
		exp = sp.getInt("PLAYER_CUR_EXP", 100);
		animal_hp_bar = (IconRoundCornerProgressBar)findViewById(R.id.iconRoundCornerProgressBar1);
		animal_hp_bar.setMax(150);
		animal_hp_bar.setProgress(animal_hp);
		animal_hp_bar.setProgressColor(selectColor(animal_hp,150));
		animal_hp_bar.setHeaderColor(selectColorHeader(animal_hp,150));
		
		hp_bar = (IconRoundCornerProgressBar) findViewById(R.id.iconRoundCornerProgressBar2);
		hp_bar.setProgressColor(Color.rgb(249, 149, 184));
		hp_bar.setBackgroundColor(Color.DKGRAY);
		hp_bar.setHeaderColor(Color.rgb(255, 43, 112));
		hp_bar.setMax(hp_max);
		hp_bar.setProgress(hp);

		mana_bar = (IconRoundCornerProgressBar) findViewById(R.id.iconRoundCornerProgressBar3);
		mana_bar.setProgressColor(Color.rgb(153, 217, 234));
		mana_bar.setBackgroundColor(Color.DKGRAY);
		mana_bar.setHeaderColor(Color.rgb(0, 162, 232));
		mana_bar.setMax(mana_max);
		mana_bar.setProgress(mana);

		exp_bar = (RoundCornerProgressBar) findViewById(R.id.roundCornerProgressBar1);
		exp_bar.setProgressColor(Color.rgb(255, 201, 14));
		exp_bar.setBackgroundColor(Color.DKGRAY);
		exp_bar.setMax(exp_max);
		exp_bar.setProgress(exp);
		
		fx=0;
		fy=0;
		
		OnClickListener l = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				double rad_x = Math.random()*PI*2;
//				double rad_y = Math.random()*PI;
//				double sign = Math.random();
//				
//				if(sign > 0.5)
//					rad_y *= -1;
//				ss.rx = 10*Math.cos(rad_x)*Math.sin(rad_y);
//				ss.ry = 10*Math.sin(rad_x)*Math.sin(rad_y);
//				ss.rz = 10*Math.cos(rad_y);
				
				
			}
		};
		ss.setOnClickListener(l);
		anim = AnimationUtils.loadAnimation(this, R.anim.anim_1);
		anim2 = AnimationUtils.loadAnimation(this, R.anim.anim_shake);
		shoot.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(ss.isOnTarget){
					int damage = (int) Math.round(Math.random()*20 + 10);
//					double rad_x = Math.random()*PI*2;
//					double rad_y = Math.random()*PI;
//					double sign = Math.random();
					
//					if(sign > 0.5)
//						rad_y *= -1;
//					ss.rx = 10*Math.cos(rad_x)*Math.sin(rad_y);
//					ss.ry = 10*Math.sin(rad_x)*Math.sin(rad_y);
//					ss.rz = 10*Math.cos(rad_y);
					//shooting.isShoot = true;
					//shooting.damage = 10;
					Log.d("IS Target", ""+ss.isOnTarget);
					animal_hp -= damage;
					animal_hp_bar.setProgress(animal_hp);
					animal_hp_bar.setProgressColor(selectColor(animal_hp,150));
					animal_hp_bar.setHeaderColor(selectColorHeader(animal_hp,150));
					
					//anim2.start();
					animal.setAnimation(anim2);
					anim2.setRepeatCount(1);
					tt.setText("-"+damage);
					tt.setVisibility(View.VISIBLE);
					anim.start();
					tt.setAnimation(anim);
					tt.setVisibility(View.GONE);
					ss.rx = MyRandom(-200, 200);//(float) (Math.random()*350);
					ss.ry = MyRandom(-200, 200);//(float) (Math.random()*350);
					if(animal_hp <= 0){
						animal.setVisibility(View.GONE);
						animal_hp_bar.setVisibility(View.GONE);
						int n = sp.getInt("PLAYER_ANIMAL_CATCH", 0);
						n=n+1;
						editor.putInt("PLAYER_ANIMAL_CATCH",n);
						showDialog();
					}
				}
			}
		});
    }
    
    public void onResume() {
        Log.d("System","onResume");
        super.onResume();
        mCamera = Camera.open();
        sensorManager.registerListener(AccListener, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }
    
    public void onPause() {
        Log.d("System","onPause");
        super.onPause();
        mCamera.release();
    }
    
    public void surfaceChanged(SurfaceHolder arg0
            , int arg1, int arg2, int arg3) {
        Log.d("CameraSystem","surfaceChanged");
        Camera.Parameters params = mCamera.getParameters();
        List<Camera.Size> previewSize = params.getSupportedPreviewSizes();
        List<Camera.Size> pictureSize = params.getSupportedPictureSizes();
        params.setPictureSize(pictureSize.get(0).width,pictureSize.get(0).height);
        params.setPreviewSize(previewSize.get(0).width,previewSize.get(0).height);
        params.setJpegQuality(100);
        mCamera.setParameters(params);
        mCamera.setDisplayOrientation(90);
        
        try {
            mCamera.setPreviewDisplay(mPreview.getHolder());
            mCamera.startPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void surfaceCreated(SurfaceHolder arg0) {
        Log.d("CameraSystem","surfaceCreated");
        try {
            mCamera.setPreviewDisplay(mPreview.getHolder());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void surfaceDestroyed(SurfaceHolder arg0) { }
    
    SensorEventListener AccListener = new SensorEventListener() {
		double x,y;
		float degree;
		
		@Override
		public void onSensorChanged(SensorEvent event) {
			// TODO Auto-generated method stub
//			
//			x_axis.setText("X : "+event.values[0]);
//			y_axis.setText("Y : "+event.values[1]);
//			z_axis.setText("Z : "+event.values[2]);
			Render3.x = event.values[0];
			Render3.y = event.values[1];
			Render3.z = event.values[2];
//			x = event.values[0];
//			y = event.values[1];
//			//degree = (int) Math.toDegrees(Math.atan(y/x));
//			
//			fx -= event.values[0]*2;
//			fy += event.values[1]*2;
//			
//			ui.fx = fx;
//			ui.fy = fy;
//			if(fx > 200){
//				fx = 200;
//			}
//			if(fy > 200){
//				fy = 200;
//			}
//			if(fx < 0){
//				fx = 0;
//			}
//			if(fy < 0){
//				fy = 0;
//			}
//			
//			degree = (int) Math.toDegrees(Math.atan(fy/fx));
			//animal.setRotation(degree-90);
			//sc.setText("Degree : "+ degree);
		}
		
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// TODO Auto-generated method stub
			
		}
	};
	
	private int selectColor(int progress,int max){
		int percent = (int) ((progress/(max*1.0))*100.0);
		Log.d("Percent",""+percent);
		if(percent > 70){
			return Color.rgb(181, 230, 29);
		}
		if(percent > 40){
			return Color.rgb(255, 201, 14);
		}else{
			return Color.rgb(237, 28, 36);
		}
	}
	private int selectColorHeader(int progress,int max){
		int percent = (int) ((progress/(max*1.0))*100.0);
		Log.d("Percent",""+percent);
		if(percent > 70){
			return Color.rgb(141, 190, 0);
		}
		if(percent > 40){
			return Color.rgb(215, 161, 0);
		}else{
			return Color.rgb(197, 0, 0);
		}
	}
	private void showDialog(){
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.missoin_complete);
		FlatButton done = (FlatButton)dialog.findViewById(R.id.done_btn);
		String[] list = { "Aerith Gainsborough", "Barret Wallace", "Cait Sith"
                , "Cid Highwind"};
		int resId = R.drawable.calorie_icon;
		MyAdapter adapter = new MyAdapter(getApplicationContext(), list, resId, font);
		ListView listView = (ListView)dialog.findViewById(R.id.listView1);
        listView.setAdapter(adapter);
        dialog.show();
		done.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(),Main.class);
				int n = sp.getInt("PLAYER_ANIMAL_CATCH", 0);
				n = n+1;
				editor.putInt("PLAYER_ANIMAL_CATCH", n);
				editor.commit();
				dialog.dismiss();
				startActivity(i);
			}
		});
	}
	
	private float MyRandom(float lower_bound,float upper_bound){
		float check = lower_bound * upper_bound;
		float result = 0f;
		if(check < 0){
			float dl = Math.abs(0 - lower_bound)/Math.abs(upper_bound - lower_bound);
			float dh = Math.abs(upper_bound - 0)/Math.abs(upper_bound - lower_bound);
			float r = (float) Math.random();
			if(r < dl){
				result = (float)Math.random()*Math.abs(0 - lower_bound)*-1;
			}else{
				result = (float)Math.random()*Math.abs(upper_bound - 0);
			}
			
		}else{
			result = (float)Math.random()*Math.abs(upper_bound-lower_bound) + lower_bound;
		}
		return result;
	}
	
	private int randomAnimalHealth(int diffc){
		return diffc*300;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
