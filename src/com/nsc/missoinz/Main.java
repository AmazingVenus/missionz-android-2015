package com.nsc.missoinz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import app.potprunk.databasez.Calculate;

import com.akexorcist.roundcornerprogressbar.IconRoundCornerProgressBar;
import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.cengalabs.flatui.views.FlatButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class Main extends FragmentActivity {
	SharedPreferences sp;
	SharedPreferences.Editor editor;

	GoogleMap mMap;
	Marker mMarker;
	Marker[] animal;
	Circle myCircle;
	LocationManager lm;
	double lat, lng;
	double distance = 0;
	PolylineOptions walkLine = new PolylineOptions();
	GoogleApiClient mLocationClient;
	ArrayList<LatLng> point_data = new ArrayList<LatLng>();
	ArrayList<MarkerOptions> animal_point = new ArrayList<MarkerOptions>();
	
	int animal_count;
	DecimalFormat format = new DecimalFormat("#0.00");
	double mDeclination;
	TextView walk_dist;
	TextView walk_bearing;
	TextView mode_option;
	SensorManager sensorManager;
	Sensor sensor;
	boolean isInit;
	Button refresh_btn;
	Button Me_btn;
	IconRoundCornerProgressBar hp_bar;
	IconRoundCornerProgressBar mana_bar;
	RoundCornerProgressBar exp_bar;
	int mana;
	int hp;
	int exp;
	int hp_max;
	int mana_max;
	int exp_max;
	int player_weight;
	FlatButton ingame;
	Bitmap animal_c;
	Bitmap animal_b;
	Bitmap animal_a;
	Bitmap animal_s;
	Bitmap heart;

	@SuppressWarnings("deprecation")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
		 boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if(!statusOfGPS){
			showAlertDialog(Main.this, "GPS is disable",
                    "You don't have GPS Enable. Open GPS?", false);
		}
		
		sp = getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);
		editor = sp.edit();
		animal_count = sp.getInt("DAILY_ANIMAL", 10) - sp.getInt("PLAYER_ANIMAL_CATCH",0);
		if(animal_count <= 0){
			animal = new Marker[2];
		}else{
			animal = new Marker[animal_count + 31];
		}
		
		mMap = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map)).getMap();

		mLocationClient = new GoogleApiClient.Builder(this)
				.addApi(LocationServices.API).addConnectionCallbacks(mCallback)
				.addOnConnectionFailedListener(mListener).build();

		mode_option = (TextView) findViewById(R.id.textView3);
		walk_dist = (TextView) findViewById(R.id.test);
		walk_bearing = (TextView) findViewById(R.id.message);
		refresh_btn = (Button) findViewById(R.id.ok_btn);
		refresh_btn.setText(sp.getString("MODE", "Unknow"));
		sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		ingame = (FlatButton) findViewById(R.id.flatButton6);
		Me_btn = (Button) findViewById(R.id.Button2);
		walk_dist.setTextColor(Color.DKGRAY);
		hp_max = sp.getInt("PLAYER_MAX_HP", 100);
		mana_max = sp.getInt("PLAYER_MAX_STA", 100);
		exp_max = sp.getInt("PLAYER_MAX_EXP", 100);
		player_weight = sp.getInt("PLAYER_WEIGHT",50);
		mode_option.setText("Mode Level : " + sp.getInt("MODE_OPTION", -1));
		mana = sp.getInt("PLAYER_CUR_STA", 100);
		hp = sp.getInt("PLAYER_CUR_HP", 100);
		exp = sp.getInt("PLAYER_CUR_EXP", 100);

		hp_bar = (IconRoundCornerProgressBar) findViewById(R.id.iconRoundCornerProgressBar1);
		hp_bar.setProgressColor(Color.rgb(249, 149, 184));
		hp_bar.setBackgroundColor(Color.DKGRAY);
		hp_bar.setHeaderColor(Color.rgb(255, 43, 112));
		hp_bar.setMax(hp_max);
		hp_bar.setProgress(hp);

		mana_bar = (IconRoundCornerProgressBar) findViewById(R.id.iconRoundCornerProgressBar2);
		mana_bar.setProgressColor(Color.rgb(153, 217, 234));
		mana_bar.setBackgroundColor(Color.DKGRAY);
		mana_bar.setHeaderColor(Color.rgb(0, 162, 232));
		mana_bar.setMax(mana_max);
		mana_bar.setProgress(mana);

		exp_bar = (RoundCornerProgressBar) findViewById(R.id.roundCornerProgressBar1);
		exp_bar.setProgressColor(Color.rgb(255, 201, 14));
		exp_bar.setBackgroundColor(Color.DKGRAY);
		exp_bar.setMax(exp_max);
		exp_bar.setProgress(exp);

		animal_c = BitmapFactory.decodeResource(getResources(),
				R.drawable.animal_class_c64);
		animal_c = Bitmap.createScaledBitmap(animal_c, 50, 50, false);

		animal_b = BitmapFactory.decodeResource(getResources(),
				R.drawable.animal_class_b64);
		animal_b = Bitmap.createScaledBitmap(animal_b, 50, 50, false);

		animal_a = BitmapFactory.decodeResource(getResources(),
				R.drawable.animal_class_a64);
		animal_a = Bitmap.createScaledBitmap(animal_a, 50, 50, false);

		animal_s = BitmapFactory.decodeResource(getResources(),
				R.drawable.animal_class_s64);
		animal_s = Bitmap.createScaledBitmap(animal_s, 50, 50, false);

		heart = BitmapFactory.decodeResource(getResources(), R.drawable.heart);
		heart = Bitmap.createScaledBitmap(heart, 50, 50, false);
		isInit = true;
		
		refresh_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				isInit = true;
				Intent i = getBaseContext().getPackageManager()
						.getLaunchIntentForPackage(
								getBaseContext().getPackageName());
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);

			}
		});

		ingame.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(), GamePlay.class);
				startActivity(i);
			}
		});

		mMap.getUiSettings().setRotateGesturesEnabled(false);

		mMap.setOnMarkerClickListener(new OnMarkerClickListener() {

			@Override
			public boolean onMarkerClick(Marker marker) {
				// TODO Auto-generated method stub
				Location source = new Location("");
				Location dest = new Location("");
				source.setLatitude(marker.getPosition().latitude);
				source.setLongitude(marker.getPosition().longitude);

				dest.setLatitude(lat);
				dest.setLongitude(lng);
				if (source.distanceTo(dest) <= 500) {
					if (marker.getTitle().equals("Item")) {
						hp += 50;
						marker.remove();
						hp_bar.setProgress(hp);
					} else if(marker.getTitle().equals("Player")){
						hp -= 50;
						hp_bar.setProgress(hp);
					}else {
						hp -= 50;
						hp_bar.setProgress(hp);
						marker.remove();
						animal_count--;
						Intent i = new Intent(getApplicationContext(),
								GamePlay.class);
						startActivity(i);
					}
				}
				// marker.showInfoWindow();
				return false;
			}
		});

		Me_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (sp.getString("MODE", "Unknow").equals("HEALTH")) {
					Intent i = new Intent(getApplicationContext(),
							MeHealthMode.class);
					startActivity(i);
				} else if (sp.getString("MODE", "Unknow").equals("ARCADE")) {
					Intent i = new Intent(getApplicationContext(),
							MeArcadeMode.class);
					startActivity(i);
				}

			}
		});
	}

	private ConnectionCallbacks mCallback = new ConnectionCallbacks() {
		public void onConnected(Bundle bundle) {
			Toast.makeText(Main.this, "Services connected", Toast.LENGTH_SHORT)
					.show();

			LocationRequest mRequest = new LocationRequest()
					.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
					.setInterval(4000).setFastestInterval(1000);
			LocationServices.FusedLocationApi.requestLocationUpdates(
					mLocationClient, mRequest, locationListener);

			// mLocationClient.re
			// requestLocationUpdates(mRequest, locationListener);
		}

		public void onDisconnected() {
			Toast.makeText(Main.this, "Services disconnected",
					Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onConnectionSuspended(int arg0) {
			// TODO Auto-generated method stub

		}
	};

	private OnConnectionFailedListener mListener = new OnConnectionFailedListener() {
		public void onConnectionFailed(ConnectionResult result) {
			Toast.makeText(Main.this, "Services connection failed",
					Toast.LENGTH_SHORT).show();
		}
	};

	private LocationListener locationListener = new LocationListener() {
		public void onLocationChanged(Location location) {
			LatLng coordinate = new LatLng(location.getLatitude(),
					location.getLongitude());
			if (isInit) {
				mMap.clear();
				animal_point.clear();
				Log.d("Animal Position", "current : " + location.getLatitude()
						+ ", " + location.getLongitude());

				// /// Random General Animal position
				for (int i = 0; i < animal_count; i++) {
					double direction = Math.random() * 360;
					double distancee = (Math.random() * 150) + 40 + 70
							* sp.getInt("MODE_OPTION", 1);
					Log.d("Dist", "" + distancee);
					double latt = (Math.cos(direction * (Math.PI / 180)) * distancee) / 200000;
					double lngt = (Math.sin(direction * (Math.PI / 180)) * distancee) / 200000;

					latt += location.getLatitude();
					lngt += location.getLongitude();
					int mode = sp.getInt("MODE_OPTION", 1);
					float r = (float) Math.random();
					Bitmap n = null;
					switch (mode) {

					case 1:
						if (r > 0.4f) {
							n = animal_c;
						} else if (r > 0.1f) {
							n = animal_b;
						} else {
							n = animal_a;
						}
						break;
					case 2:
						if (r > 0.67f) {
							n = animal_c;
						} else if (r > 0.33f) {
							n = animal_b;
						} else {
							n = animal_a;
						}
						break;
					case 3:
						if (r > 0.9f) {
							n = animal_c;
						} else if (r > 0.7f) {
							n = animal_b;
						} else {
							n = animal_a;
						}
						break;
					}
					animal_point.add(new MarkerOptions()
							.position(new LatLng(latt, lngt))
							.title("Animal " + i)
							.icon(BitmapDescriptorFactory.fromBitmap(n)));

					Log.d("Animal Position", "point" + i + " : " + latt + ", "
							+ lngt);
				}

				// Random Special Animal
				for (int j = 0; j < 10; j++) {
					double direction = Math.random() * 360;
					double distancee = (Math.random() * 150) + 80 + 100
							* sp.getInt("MODE_OPTION", 1);
					Log.d("Dist", "" + distancee);
					double latt = (Math.cos(direction * (Math.PI / 180)) * distancee) / 200000;
					double lngt = (Math.sin(direction * (Math.PI / 180)) * distancee) / 200000;

					latt += location.getLatitude();
					lngt += location.getLongitude();
					animal_point
							.add(new MarkerOptions()
									.position(new LatLng(latt, lngt))
									.title("Special Animal " + j)
									.icon(BitmapDescriptorFactory
											.fromBitmap(animal_s)));
				}

				
				// Random heart
				for (int j = 0; j < 20; j++) {
					double direction = Math.random() * 360;
					double distancee = (Math.random() * 500);
					Log.d("Dist", "" + distancee);
					double latt = (Math.cos(direction * (Math.PI / 180)) * distancee) / 200000;
					double lngt = (Math.sin(direction * (Math.PI / 180)) * distancee) / 200000;

					latt += location.getLatitude();
					lngt += location.getLongitude();
					animal_point
							.add(new MarkerOptions()
									.position(new LatLng(latt, lngt))
									.title("Item")
									.icon(BitmapDescriptorFactory
											.fromBitmap(heart)));
				}
				
				
				for (int j = 0; j < animal_point.size(); j++) {
					animal[j] = mMap.addMarker(animal_point.get(j));
				}

				isInit = false;
			}

			lat = location.getLatitude();
			lng = location.getLongitude();

			if (point_data.size() < 2) {
				point_data.add(coordinate);
			} else {
				point_data.remove(0);
				point_data.add(coordinate);
				Location source = new Location("");
				Location dest = new Location("");
				source.setLatitude(point_data.get(0).latitude);
				source.setLongitude(point_data.get(0).longitude);

				dest.setLatitude(point_data.get(1).latitude);
				dest.setLongitude(point_data.get(1).longitude);

				if (source.distanceTo(dest) > 3) {
					distance += source.distanceTo(dest);
					editor.putInt("PLAYER_WALKED_TOTAL",
							(int) Math.round(distance));
					int cal_burned = (int) Calculate.calcCalorieBurnedPerMiniute(3.5f, player_weight, (4f/60f))+sp.getInt("PLAYER_CAL_BURNED", 0);
					editor.putInt("PLAYER_CAL_BURNED", cal_burned);
					editor.commit();
				}

				walk_dist.setText("Walked Distance : "
						+ format.format(distance) + " m.");

				// mMarker = mMap.addMarker(new
				// MarkerOptions().position(point_data.get(0)));
			}

			if (mMarker != null)
				mMarker.remove();

			// mMarker = mMap.addMarker(new MarkerOptions().position(new
			// LatLng(lat, lng)));

			// mMarker = mMap.addMarker(new
			// MarkerOptions().position(coordinate).icon(BitmapDescriptorFactory.fromAsset("androidpointer080814.png")));

			mMap.animateCamera(CameraUpdateFactory
					.newLatLngZoom(coordinate, 17));

			walkLine.add(coordinate).width((float) 5)
					.color(Color.rgb(247, 141, 64));
			mMap.addPolyline(walkLine);
			CircleOptions c = new CircleOptions().center(coordinate).radius(50)
					.strokeColor(Color.rgb(13, 162, 255))
					.fillColor(Color.argb(30, 13, 162, 255)).strokeWidth(5);
//
//			CircleOptions c1 = new CircleOptions().center(coordinate)
//					.radius(100).strokeColor(Color.rgb(255, 0, 0))
//					.fillColor(Color.argb(0, 13, 162, 255)).strokeWidth(5);
//
//			CircleOptions c2 = new CircleOptions().center(coordinate)
//					.radius(150).strokeColor(Color.rgb(0, 255, 0))
//					.fillColor(Color.argb(0, 13, 162, 255)).strokeWidth(5);
//
//			CircleOptions c3 = new CircleOptions().center(coordinate)
//					.radius(200).strokeColor(Color.rgb(0, 0, 255))
//					.fillColor(Color.argb(0, 13, 162, 255)).strokeWidth(5);
//			CircleOptions c4 = new CircleOptions().center(coordinate)
//					.radius(200).strokeColor(Color.argb(0, 0, 0, 255))
//					.fillColor(Color.argb(0, 13, 162, 255)).strokeWidth(5);

			if (myCircle != null)
				myCircle.remove();

				myCircle = mMap.addCircle(c);
//				myCircle = mMap.addCircle(c1);
//				myCircle = mMap.addCircle(c2);
//				myCircle = mMap.addCircle(c3);
//				myCircle = mMap.addCircle(c4);

		}

	};

	protected void onStart() {
		super.onStart();
		mLocationClient.connect();
	}

	protected void onStop() {
		super.onStop();
		mLocationClient.disconnect();
	}

	SensorEventListener magnetListener = new SensorEventListener() {

		@Override
		public void onSensorChanged(SensorEvent event) {
			// TODO Auto-generated method stub
			walk_bearing.setText("X : " + format.format(event.values[0])
					+ ", Y : " + format.format(event.values[1]) + ", Z : "
					+ format.format(event.values[2]));
			if (mMarker != null)
				mMarker.remove();
			mMarker = mMap.addMarker(new MarkerOptions()
					.rotation(event.values[0])
					.anchor(0.5f, 0.5f)
					.position(new LatLng(lat, lng))
					.icon(BitmapDescriptorFactory
							.fromAsset("direc-marker-40.png")));
			mMarker.setTitle("Player");
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// TODO Auto-generated method stub

		}
	};

	public void onResume() {
		super.onResume();
		sensorManager.registerListener(magnetListener, sensor,
				SensorManager.SENSOR_DELAY_NORMAL);
		
	}

	private Animal[] getAnimalRandom(int count, int mode, LatLng player_pos,
			GoogleMap Map) {
		Animal[] rand_animal = new Animal[count];

		for (int i = 0; i < count; i++) {
			double direction = Math.random() * 360;
			double distancee = (Math.random() * 150) + 40 + 70
					* sp.getInt("MODE_OPTION", 1);
			Log.d("Dist", "" + distancee);
			double latt = (Math.cos(direction * (Math.PI / 180)) * distancee) / 200000;
			double lngt = (Math.sin(direction * (Math.PI / 180)) * distancee) / 200000;

			latt += player_pos.latitude;
			lngt += player_pos.longitude;
			float r = (float) Math.random();
			Bitmap n = null;
			switch (mode) {

			case 1:
				if (r > 0.4f) {
					n = animal_c;
					rand_animal[i].setAnimalLevel(1);
					rand_animal[i]
							.setAnimalHealth((int) (Math.random() * 100 + 100));
					rand_animal[i]
							.setAnimalExp((int) (Math.random() * 200 + 200));
				} else if (r > 0.1f) {
					n = animal_b;
					rand_animal[i].setAnimalLevel(2);
					rand_animal[i]
							.setAnimalHealth((int) (Math.random() * 100 + 200));
					rand_animal[i]
							.setAnimalExp((int) (Math.random() * 200 + 400));
				} else {
					n = animal_a;
					rand_animal[i].setAnimalLevel(3);
					rand_animal[i]
							.setAnimalHealth((int) (Math.random() * 100 + 300));
					rand_animal[i]
							.setAnimalExp((int) (Math.random() * 200 + 600));
				}
				break;
			case 2:
				if (r > 0.67f) {
					n = animal_c;
					rand_animal[i].setAnimalLevel(1);
					rand_animal[i]
							.setAnimalHealth((int) (Math.random() * 100 + 100));
					rand_animal[i]
							.setAnimalExp((int) (Math.random() * 200 + 200));
				} else if (r > 0.33f) {
					n = animal_b;
					rand_animal[i].setAnimalLevel(2);
					rand_animal[i]
							.setAnimalHealth((int) (Math.random() * 100 + 200));
					rand_animal[i]
							.setAnimalExp((int) (Math.random() * 200 + 400));
				} else {
					n = animal_a;
					rand_animal[i].setAnimalLevel(3);
					rand_animal[i]
							.setAnimalHealth((int) (Math.random() * 100 + 300));
					rand_animal[i]
							.setAnimalExp((int) (Math.random() * 200 + 600));
				}
				break;
			case 3:
				if (r > 0.9f) {
					n = animal_c;
					rand_animal[i].setAnimalLevel(1);
					rand_animal[i]
							.setAnimalHealth((int) (Math.random() * 100 + 100));
					rand_animal[i]
							.setAnimalExp((int) (Math.random() * 200 + 200));
				} else if (r > 0.7f) {
					n = animal_b;
					rand_animal[i].setAnimalLevel(2);
					rand_animal[i]
							.setAnimalHealth((int) (Math.random() * 100 + 200));
					rand_animal[i]
							.setAnimalExp((int) (Math.random() * 200 + 400));
				} else {
					n = animal_a;
					rand_animal[i].setAnimalLevel(3);
					rand_animal[i]
							.setAnimalHealth((int) (Math.random() * 100 + 300));
					rand_animal[i]
							.setAnimalExp((int) (Math.random() * 200 + 600));
				}
				break;
			}
			MarkerOptions animal_opt = new MarkerOptions()
					.position(new LatLng(latt, lngt)).title("Animal " + i)
					.icon(BitmapDescriptorFactory.fromBitmap(n));
			Marker animal_mark = Map.addMarker(animal_opt);
			rand_animal[i].setAnimalPos(animal_mark);

			Log.d("Animal Position", "point" + i + " : "
					+ rand_animal[i].getAnimalPos().getPosition().latitude
					+ ", "
					+ rand_animal[i].getAnimalPos().getPosition().longitude);

		}
		return rand_animal;
	}

	// private Animal randomAnimalClassC(){
	// int hp = (int) (Math.random()*100+100);
	// int exp = (int) (Math.random()*200+200);
	// Animal a = new Animal(hp, exp, 1, null);
	// return a;
	//
	// }

	private String getRandomAnimalName() {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(getAssets().open(
					"animal_name.txt"), "UTF-8"));

			// do reading, usually loop until end of file reading
			String mLine = reader.readLine();
			while (mLine != null) {
				mLine = reader.readLine();
			}
		} catch (IOException e) {
			// log the exception
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// log the exception
				}
			}
		}
		return null;

	}

	private ArrayList AnimalAdaptor(Animal[] source) {
		return animal_point;

	}
	public void showAlertDialog(Context context, String title, String message, final Boolean status) {
//      AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//
//      // Setting Dialog Title
//      alertDialog.setTitle(title);
//
//      // Setting Dialog Message
//      alertDialog.setMessage(message);
//       
//      // Setting alert dialog icon
//      alertDialog.setIcon((status) ? R.drawable.success_icon : R.drawable.fail_icon);
//
//      // Setting OK Button
//      alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
//          public void onClick(DialogInterface dialog, int which) {
//          }
//      });
//
//      // Showing Alert Message
//      alertDialog.show();
		
		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.connection_dialog);
		TextView title_txt = (TextView)dialog.findViewById(R.id.title);
		TextView msg_txt = (TextView)dialog.findViewById(R.id.message);
		FlatButton conect_ok_btn = (FlatButton)dialog.findViewById(R.id.connect_ok);
		ImageView icon = (ImageView)dialog.findViewById(R.id.imageView1);
		icon.setImageResource((status) ? R.drawable.success_icon : R.drawable.fail_icon);
		title_txt.setText(title);
		msg_txt.setText(message);
		//msg_txt.setTypeface(font);
		
		conect_ok_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!status)
					startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
				dialog.dismiss();
			}
		});
		dialog.show();
  }
}
