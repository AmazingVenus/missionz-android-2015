package com.nsc.missoinz;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MeArcadeMode extends Activity{
	SharedPreferences sp;
	SharedPreferences.Editor editor;
	
	
	TextView animal_goal_txt;
    TextView animal_catch_txt;
    
    TextView exp_goal_txt;
    TextView exp_earned_txt;
    
    
    TextView cal_burned_txt;
    TextView dist_walked_txt;
    
    TextView score_earned_txt;
    
    TextView title_txt;
    TextView level_info_txt;
    RoundCornerProgressBar exp_bar;
    RoundCornerProgressBar animal_bar;
    
    int cal_burned;
    int dist_walked;
    int animal_catch;
    int score = 123;
    int exp = 145;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.me_arcade);
        
        sp = getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);
        dist_walked = sp.getInt("PLAYER_WALKED_TOTAL", 150);
        
        title_txt = (TextView)findViewById(R.id.test);
        level_info_txt = (TextView)findViewById(R.id.message);
        
        animal_goal_txt = (TextView)findViewById(R.id.textView3);
        animal_catch_txt = (TextView)findViewById(R.id.textView4);
        
        exp_goal_txt = (TextView)findViewById(R.id.textView5);
        exp_earned_txt = (TextView)findViewById(R.id.textView6);
        
        cal_burned_txt = (TextView)findViewById(R.id.textView8);
        dist_walked_txt = (TextView)findViewById(R.id.textView9);
        score_earned_txt = (TextView)findViewById(R.id.textView10);
        
        
        exp_bar = (RoundCornerProgressBar)findViewById(R.id.roundCornerProgressBar2);
        exp_bar.setMax(sp.getInt("DAILY_ANIMAL", 0)*20);
        exp_bar.setProgress(animal_catch*20);
        exp_bar.setProgressColor(selectColor((animal_catch*20), sp.getInt("DAILY_ANIMAL", 0)*20));
        
        animal_catch = sp.getInt("PLAYER_ANIMAL_CATCH", 0);
        animal_bar = (RoundCornerProgressBar)findViewById(R.id.roundCornerProgressBar1);
        animal_bar.setMax(sp.getInt("DAILY_ANIMAL", 20));
        animal_bar.setProgress(animal_catch);
        animal_bar.setProgressColor(selectColor(animal_catch, sp.getInt("DAILY_ANIMAL", 20)));
        
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/Arabica.ttf");
        level_info_txt.setTypeface(font);
        exp_goal_txt.setTypeface(font);
        cal_burned_txt.setTypeface(font);
        dist_walked_txt.setTypeface(font);
        animal_catch_txt.setTypeface(font);
        animal_goal_txt.setTypeface(font);
        score_earned_txt.setTypeface(font);
        exp_earned_txt.setTypeface(font);
        title_txt.setTypeface(font);
        
        //title_txt.setTextSize(50);
        level_info_txt.setTextSize(40);
        //exp_goal_txt.setTextSize(17);
        cal_burned_txt.setTextSize(20);
        dist_walked_txt.setTextSize(20);
        animal_catch_txt.setTextSize(17);
        animal_goal_txt.setTextSize(17);
        score_earned_txt.setTextSize(20);
        exp_earned_txt.setTextSize(17);
        cal_burned = sp.getInt("PLAYER_CAL_BURNED", 0);
        level_info_txt.setText("Level 1 : " + sp.getString("PLAYER_NAME", "Unkhow"));
        
        animal_goal_txt.setText("������� : �ӹǹ�ѵ�����ͧ�Ѻ\t\t\t" + sp.getInt("DAILY_ANIMAL", 10) + " ���");
        animal_catch_txt.setText("�ӹǹ�ѵ����Ѻ��\t\t\t\t\t\t\t\t\t\t" + animal_catch + " ���");
        
        exp_goal_txt.setText("������� : ��һ��ʺ��ó�\t\t\t\t" + (sp.getInt("DAILY_ANIMAL", 0)*20) +" exp");
        exp_earned_txt.setText("��һ��ʺ��ó������Ѻ\t\t\t\t\t\t\t\t" + (animal_catch*20)+" exp");
        
        dist_walked_txt.setText("���зҧ����Թ�\t\t\t\t\t\t\t\t\t\t\t\t\t" + dist_walked + " ����");
        cal_burned_txt.setText("���������Ҽ�ҭ��\t\t\t\t\t\t\t\t\t\t" + cal_burned+" ������");
         
        score_earned_txt.setText("��ṹ������Ѻ\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+score+" ��ṹ");
        
	}
	
	private int selectColor(int progress,int max){
		int percent = (int) ((progress/(max*1.0))*100.0);
		Log.d("Percent",""+percent);
		if(percent > 70){
			return Color.rgb(181, 230, 29);
		}
		if(percent > 40){
			return Color.rgb(255, 201, 14);
		}else{
			return Color.rgb(237, 28, 36);
		}
	}
}
