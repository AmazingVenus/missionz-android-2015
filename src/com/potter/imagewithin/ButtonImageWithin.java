package com.potter.imagewithin;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nsc.missoinz.R;

public class ButtonImageWithin extends LinearLayout{
	private ImageView imageIcon;
	private TextView text;
	private int radius = 10;
	private int backgroundColor = Color.parseColor("#595959");
	private LinearLayout layoutBackground;
	private int padding = 5;
	public ButtonImageWithin(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.image_within, this);
		setup(context, attrs);
	}

	private void setup(Context context, AttributeSet attrs) {
		imageIcon = (ImageView) findViewById(R.id.imageView1);
		text = (TextView) findViewById(R.id.title);
		layoutBackground = (LinearLayout) findViewById(R.id.background_layout);
		layoutBackground.setPadding(padding+5, padding, padding+5, padding);
	}
	
	public void setText(String txt){
		this.text.setText(txt);
	}

	public void setImage(int resID){
		this.imageIcon.setImageResource(resID);
	}
	@TargetApi(Build.VERSION_CODES.KITKAT)
	@SuppressLint("NewApi")
	public void setIconSize(int w,int h){
		Bitmap resize = BitmapFactory.decodeResource(getResources(), (Integer) imageIcon.getTag());
		resize.setHeight(h);
		resize.setWidth(w);
		imageIcon.setImageBitmap(resize);
	}
	@SuppressLint("NewApi")
	public void setButtonColor(int color){
		backgroundColor = color;
		GradientDrawable gradient = new GradientDrawable();
		gradient.setShape(GradientDrawable.RECTANGLE);
		gradient.setColor(backgroundColor);
		gradient.setCornerRadius(radius);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			layoutBackground.setBackgroundDrawable(gradient);
    	} else {
			layoutBackground.setBackground(gradient);
    	}
	}
	
}
